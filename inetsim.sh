#!/bin/bash

yum update -y
rpm -ivh http://mirror.pnl.gov/epel/6/i386/epel-release-6-8.noarch.rpm
rpm --import https://fedoraproject.org/static/0608B895.txt
yum install -y perl-ExtUtils-MakeMaker perl-Net-Server perl-Digest-SHA perl-Time-HiRes perl-Net-DNS perl-Net-DNS-Nameserver

cd /opt
wget http://www.inetsim.org/downloads/inetsim-1.2.3.tar.gz
tar xvzf inetsim-1.2.3.tar.gz
cd inetsim-1.2.3

groupadd inetsim
./setup.sh

cd /tmp
wget http://search.cpan.org/CPAN/authors/id/M/MS/MSOUTH/IPC-Shareable-0.61.tar.gz
tar xvzf IPC-Shareable-0.61.tar.gz
cd IPC-Shareable-0.61
perl Makefile.PL
make
make install

cd /tmp
wget http://www.net-dns.org/download/Net-DNS-0.72.tar.gz
tar xvzf Net-DNS-0.72.tar.gz
cd Net-DNS-0.72
perl Makefile.PL
make 
make install

service postfix stop
chkconfig postfix off
service ntpd stop
chkconfig ntpd off

iptables -F
echo '' > /etc/sysconfig/iptables
